package com.gmail.gfmx2048.fontchangabletextview;

import android.content.Context;
import android.graphics.Typeface;
import android.widget.TextView;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Vasilis Viktoratos on 13/9/2017.
 */

public class FontUtil {

    //for caching
    private static final Map<String,Typeface> FONTS=new HashMap<>();

    //search the typeface on assets folder and on subfolder fonts
    public static Typeface getTypeFace(Context context,String typefaceName){
       Typeface typeface=FONTS.get(typefaceName);
        if(typeface==null){
            typeface = Typeface.createFromAsset(context.getAssets(),
                    "fonts/"+typefaceName);
            FONTS.put(typefaceName,typeface);

        }
        return typeface;


    }


    public static void setTypeface(TextView view, String typefaceName) {
        view.setTypeface(getTypeFace(view.getContext(),typefaceName));
    }
}
