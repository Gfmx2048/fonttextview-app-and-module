package com.gmail.gfmx2048.fontchangabletextview;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.annotation.Nullable;
import android.util.AttributeSet;

/**
 * Created by Vasilis Viktoratos on 13/9/2017.
 */

public class FontTextView extends android.support.v7.widget.AppCompatTextView {
    public FontTextView(Context context) {
        super(context);
        init(context, null, 0);
    }

    public FontTextView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs, 0);
    }

    public FontTextView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs, defStyleAttr);
    }

    private void init(Context context, AttributeSet attrs, int defStyle) {
        if (attrs != null) {
            final TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.FontTextView, defStyle, 0);
            final String typeface = a.getString(R.styleable.FontTextView_fontName);
            a.recycle();

            if (typeface != null) {
                setTypeFace(typeface);
            }
        }
    }

    public void setTypeFace(String typeFaceName) {
        FontUtil.setTypeface(this, typeFaceName);
    }


}
